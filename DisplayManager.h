#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class DisplayManager {
public:
    GLFWwindow* window;

    // Movement control
    static bool firstMouse = true;
    static bool keys[1024];

    int createDisplay(){
        // GLFW Initialization
        glfwInit();
        // Set GLFW options
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

        // Create the game window object
        this->window = glfwCreateWindow(WIDTH, HEIGHT, "One v0.0", NULL, NULL);
        if (this->window == NULL) {
            printf("Failed to create GLFW Window");
            glfwTerminate();
            return -1;
        }
        glfwMakeContextCurrent(this->window);
        glfwSetWindowPos(this->window, 100, 100);

        // Set cursor options
        glfwSetInputMode(this->window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        // Register callback function for keypress and mouse
        glfwSetKeyCallback(this->window, key_callback);
        glfwSetCursorPosCallback(this->window, mouse_callback);
        glfwSetScrollCallback(this->window, scroll_callback);

        // Set GLEW for modern OpenGL bindings and initialize
        glewExperimental = GL_TRUE;
        if (glewInit() != GLEW_OK) {
            printf("Failed to initialize GLEW");
            return -1;
        }

        // Define viewport dimensions
        //glfwGetFramebufferSize(window, WIDTH, HEIGHT);
        glViewport(0, 0, WIDTH, HEIGHT);

        // Set up OpenGL options
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);
        glEnable(GL_CULL_FACE);
    }

    void updateDisplay() {
        // Swap buffers
        glfwSwapBuffers(this->window);
    }

    void closeDisplay() {
        // clear GLFW resources
        glfwTerminate();
    }

    // Called whenever a key is pressed/released via GLFW
    static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode) {
        // When a user presses the escape key, we set the WindowShouldClose property to true,
        // closing the application
        if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

        // When user presses another key
        if (key >=0 && key < 1024) {
            if(action == GLFW_PRESS)
            keys[key] = true;
            else if(action == GLFW_RELEASE)
            keys[key] = false;
        }
    }

    // Mouse callback
    static void mouse_callback(GLFWwindow* window, double xpos, double ypos)
    {
        if(firstMouse)
        {
            lastX = xpos;
            lastY = ypos;
            firstMouse = false;
        }

        GLfloat xoffset = xpos - lastX;
        GLfloat yoffset = lastY - ypos;

        lastX = xpos;
        lastY = ypos;

        player.processMouseMove(xoffset, yoffset);
        // camera.processMouseMove(xoffset, yoffset);
    }

    // Mouse scroll
    static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
    {
        camera.processMouseScroll(yoffset);
    }
private:
    // Window dimensions
    const GLuint WIDTH = 1280, HEIGHT = 720;

};
