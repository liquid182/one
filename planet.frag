#version 330 core

in vec4 Normal;
in vec3 FragPos;

out vec4 color;

uniform vec3 lightPos;
uniform vec3 lightColor;
//uniform vec3 objectColor;

void main() {
  // color
  vec3 objectColor = vec3(0.9f, 0.5f, 0.2f);
  // Ambient
  float ambientStrength = 0.2f;
  vec3 ambient = ambientStrength * lightColor;

  // Diffuse
  // vec3 norm1 = {Normal.x, Normal.y, Normal.z};
  // vec3 norm = normalize(Normal.x, Normal.y, Normal.z);  // should be done already
  vec3 norm = normalize(Normal.xyz);
  vec3 lightDir = normalize(lightPos - FragPos);
  float diff = max(dot(norm, lightDir), 0.0f);
  // float diff = dot(norm, lightDir);
  vec3 diffuse = 1.0f * diff * lightColor;

  vec3 result = (ambient + diffuse) * objectColor;
  color = vec4(result, 1.0f);
}
