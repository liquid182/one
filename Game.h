/*******************************************************************
| Game class header file
*******************************************************************/
#ifndef GAME_H
#define GAME_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

// toggle game state
enum GameState {
  GAME_ACTIVE,
  GAME_MENU,
  GAME_WIN
};

// The Game class holds all game-related state and functionality
class Game {
public:
  // Game state
  GameState State;
  GLboolean Keys[1024];
  GLuint Width, Height;

  // Constructor/Destructor
  Game(GLuint width, GLuint height);
  ~Game();

  // Initialize game state (ie load shaders/textures/models/world)
  void gameInit();

  // Game Loop
  void loopProcessInput(GLfloat dt);
  void loopUpdate(GLfloat dt);
  void loopRender();
};

#endif
